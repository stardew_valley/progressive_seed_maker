# Progressive Seed Maker
Version 1.1.1

Provides a simple progression system to Seed Makers. Artwork adapted from Stardew Valley's original Seed Maker sprite.

## CHANGES
* 1.1.1
    * Further adjustments to seed generation to straighten out forage.
* 1.1.0
    * Split out Flower category to generate Mixed Flower Seed instead of Mixed Seed.