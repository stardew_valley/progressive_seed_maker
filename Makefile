name = \[CP\]\ Progressive\ Seed\ Maker
folder = $(name)/
zipfile = $(name).zip

all: zip clean

clean:
	rm -rf $(folder)
	echo "Filesystem cleaned"

zip: files
	rm -f $(zipfile)
	zip -r $(zipfile) $(folder)
	echo "Zip file complete!"

files: folders
	cp LICENCE $(name)
	cp README.md $(name)
	cp manifest.json $(name)
	cp content.json $(name)
	cp -r assets/*.png $(name)"/assets/"

folders:
	mkdir -p $(name)
	mkdir -p $(name)"/assets"
